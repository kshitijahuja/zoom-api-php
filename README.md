# Zoom API PHP Library

This library is a quick solution to make API calls to Zoom.us to leverage the Zoom API v2.

## Installation

1. Extract the package file in a directory on your web server where PHP can run the files.
2. Update the config.inc.php file with your API Key and Secret.
3. Your are now set up for usage.

## Usage

Run any of the example files provided in the examples directory. Make sure to edit and update php file to include the user email / account id for whom you wish to execute the API call.

## Help and implementations
For help and/with implementations, please email at help@academicdatasolutions.com
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[GNU AGPLv3](https://choosealicense.com/licenses/agpl-3.0/)

## Disclaimer
This code is provided as is and is only intended to be used for illustration purposes. This code is not production-ready and is not meant to be used in a production environment. This repository is to be used as a tool to help you learn how to integrate with Zoom API. Any use of this repository or any of its code in a production environment is at your own risk.



--