<?php

/**
* Description: This is a basic file to perform basic API operations using the Zoom API
* Author: Kshitij Ahuja
* Version: 1.0.0
* Author URI: http://academicdatasolutions.com
* Email: help@academicdatasolutions.com
**/

require ('config.inc.php');

//B64 URL ENCODE
function base64urlencode($url) {
   return str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($url));
}

//GETS THE ACCESS TOKEN
function getAccessToken($secretKey) {
   $header = json_encode(["typ" => "JWT", "alg" => "HS256"]);
   $base64UrlHeader = base64urlencode($header);

   $payload = json_encode(["iss" => ads_zoomApiKey, "exp" => time() + 60]);
   $base64UrlPayload = base64urlencode($payload);

   $signature = hash_hmac("sha256", $base64UrlHeader . "." . $base64UrlPayload, $secretKey, true);
   $base64UrlSignature = base64urlencode($signature);

   return $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;
}


//GENERIC HTTP POST
function httpPost($url,$token,$postData)
{
    $ch = curl_init();  
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch,CURLOPT_HEADER, false); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    
	curl_setopt($ch, CURLOPT_HTTPHEADER,array(
    "authorization: Bearer $token",
    "content-type: application/json"
	));
    $output=curl_exec($ch);
    curl_close($ch);
    return $output;
}

//GENERIC HTTP GET
function httpGet($url,$token)
{
    $ch = curl_init();  
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch,CURLOPT_HEADER, false); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");                                                                     
	curl_setopt($ch, CURLOPT_HTTPHEADER,array(
    "authorization: Bearer $token",
    "content-type: application/json"
	));
    $output=curl_exec($ch); 
    curl_close($ch);
    return $output; 
}
?>