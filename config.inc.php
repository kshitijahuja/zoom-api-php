<?php

/**
* Description: Configuration file that holds API Credentials for the Zoom account
* Author: Kshitij Ahuja
* Author URI: http://academicdatasolutions.com
* Email: help@academicdatasolutions.com
**/

define("ads_zoomApiKey", "<< ENTER YOUR API KEY HERE >>");						//API key from Zoom - something like XXXxdxdxxxxxxX5_XpX_x
define("ads_zoomApiSecret", "<< ENTER YOUR API SECRET HERE >>");	//API Secret from Zoom

?>