<?php

/**
* Example - how to create a new user
* Author: Kshitij Ahuja
* Author URI: http://academicdatasolutions.com
* Email: help@academicdatasolutions.com
**/


//include the class file
require ('../zoom.class.php');

//get the token
$token = getAccessToken(ads_zoomApiSecret); //ads_zoomApiSecret can be set in config.inc.php file

//Create a user
//######################################
$params = array();
$params['action'] = 'create';
$params['user_info']['email'] = 'user@example.com';
$params['user_info']['type'] = 1;
$params['user_info']['first_name'] = 'Kshitij';
$params['user_info']['last_name'] = 'Ahuja';
$url = 'https://api.zoom.us/v2/users/';
$jsonData = json_encode($params);
echo httpPost($url, $token, $jsonData);


?>