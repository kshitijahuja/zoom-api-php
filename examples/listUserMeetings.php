<?php

/**
* Example - list all meetings for a user
* Author: Kshitij Ahuja
* Author URI: http://academicdatasolutions.com
* Email: help@academicdatasolutions.com
**/


//include the class file
require ('../zoom.class.php');

//get the token
$token = getAccessToken(ads_zoomApiSecret); //ads_zoomApiSecret can be set in config.inc.php file

//List all meetings for a user
//######################################
$url = 'https://api.zoom.us/v2/users/user@example.com/meetings'; //use user\'s email or account id 
echo httpGet($url, $token);


?>