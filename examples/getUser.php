<?php

/**
* Example - retrieve user account details
* Author: Kshitij Ahuja
* Author URI: http://academicdatasolutions.com
* Email: help@academicdatasolutions.com
**/


//include the class file
require ('../zoom.class.php');

//get the token
$token = getAccessToken(ads_zoomApiSecret); //ads_zoomApiSecret can be set in config.inc.php file


//Get User Account Details
//######################################
$url = 'https://api.zoom.us/v2/users?status=active&page_size=30&page_number=1';
echo httpGet($url, $token);


?>