<?php

/**
* Example - create a meeting
* Author: Kshitij Ahuja
* Author URI: http://academicdatasolutions.com
* Email: help@academicdatasolutions.com
**/


//include the class file
require ('../zoom.class.php');

//get the token
$token = getAccessToken(ads_zoomApiSecret); //ads_zoomApiSecret can be set in config.inc.php file

//Create a meeting
//######################################
$meetConfig = array();
$meetConfig['topic'] = 'Meeting with Kshitij';
$meetConfig['type'] = 2;
$meetConfig['start_time'] = date('yy-M-dTH:m:s'); //set only for scheduled or fixed-time meetings
$meetConfig['duration'] = 30; //minutes
$meetConfig['timezone'] = 'Asia/Kolkata';
$meetConfig['password'] = rand(1000000000,9999999999); //randomly generated 10-digit password
$meetConfig['agenda'] = 'We will discuss about how to utilize Zoom API'; //meeting description

//For recurring meetings, uncomment this array set and use (see API docs for config details)
//$meetConfig['recurrence']['type'] = 'some type';
//$meetConfig['recurrence']['repeat_interval'] = 'some type';
//$meetConfig['recurrence']['weekly_days'] = 'some type';
//$meetConfig['recurrence']['monthly_day'] = 'some type';
//$meetConfig['recurrence']['monthly_week'] = 'some type';
//$meetConfig['recurrence']['monthly_week_day'] = 'some type';
//$meetConfig['recurrence']['end_times'] = 'some type';
//$meetConfig['recurrence']['end_date_time'] = 'some type';

$meetConfig['settings']['host_video'] = true;
$meetConfig['settings']['participant_video'] = true;
$meetConfig['settings']['cn_meeting'] = false;
$meetConfig['settings']['in_meeting'] = false;
$meetConfig['settings']['join_before_host'] = true;
$meetConfig['settings']['mute_upon_entry'] = false;
$meetConfig['settings']['watermark'] = false;
$meetConfig['settings']['use_pmi'] = false;
$meetConfig['settings']['approval_type'] = 0;
//$meetConfig['settings']['registration_type'] = 'some type';
$meetConfig['settings']['audio'] = 'both';
$meetConfig['settings']['auto_recording'] = 'local';
$meetConfig['settings']['enforce_login'] = false;
$meetConfig['settings']['enforce_login_domains'] = '';
$meetConfig['settings']['alternative_hosts'] = '';
$meetConfig['settings']['registrants_email_notification'] = true;

$url = 'https://api.zoom.us/v2/users/user@example.com/meetings'; //user's email address or account id will go here
$jsonData = json_encode($meetConfig);
echo httpPost($url, $token, $jsonData);

?>